#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Ejemplo de recepción de RTP
"""

import socketserver
import time
import threading

import simplertp

IP = "127.0.0.1"
PORT = 6010


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():
    # Abrimos un fichero para escribir los datos que se reciban
    RTPHandler.open_output('output.mp3')
    with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
        print("Listening...")
        # El bucle de recepción (serv_forever) va en un hilo aparte,
        # para que se continue ejecutando este programa principal,
        # y podamos interrumpir ese bucle más adelante
        threading.Thread(target=serv.serve_forever).start()
        # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
        # por ejemplo
        time.sleep(30)
        print("Time passed, shutting down receiver loop.")
        # Paramos el bucle de recepción, con lo que terminará el thread,
        # y dejamos de recibir paquetes
        serv.shutdown()
    # Cerramos el fichero donde estamos escribiendo los datos recibidos
    RTPHandler.close_output()


if __name__ == "__main__":
    main()
